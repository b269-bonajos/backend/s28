// insertMany methods
db.rooms.insertMany([
	{
		name: "single",
		acommodates: "2",
		price: "1000",
		description: "A simple room with all the basic necessities",
		rooms_available: "10",
		isAvailable: "false"
	},
	{
		name: "double",
		acommodates: "3",
		price: "2000",
		description: "A room fit for a small family going on a vacation",
		rooms_available: "5",
		isAvailable: "false"
	},
	{
		name: "queen",
		acommodates: "4",
		price: "4000",
		description: "A room with queen sized bed perfect for simple getaway",
		rooms_available: "15",
		isAvailable: "false"
	}

]);


// find method
db.rooms.find({
	name: "double"
});


// updateOne Method
db.rooms.updateOne(
	{name: "queen"},
	{
		$set: {
			name: "queen",
			acommodates: "4",
			price: "4000",
			description: "A room with queen sized bed perfect for simple getaway",
			rooms_available: "0",
			isAvailable: "false"
		}
	}

);


// delteMany methods
db.rooms.deleteOne({name: "queen"});